
//TicTacToe
//_______________________________________________
//ERRORS
//TRY/CATCH 
//_______________________________________________

#include <iostream>
#include <stdlib.h>

#define GRIDSIZE 3 
const int GAMEGRIDSIZE = (GRIDSIZE*GRIDSIZE);
static char** gameGrid;
static bool player = 0;
static int turnCounter = 0;
static int row = 0;
static int col = 0;
static char currPlayer = 0;

void printGrid();
void playAgain();
void validateColData();
void validateRowData();
int getColInput();
int getRowInput();
void playerInputLoop();
void gameLogic();
int getTurnCounter();
int setTurnCounter();
bool setPlayerMove();
void setCol(int);
void setRow(int);
int getCol();
int getRow();
bool getPlayer();
void gameLoop();
void resetGrid();
void resetGame();
void switchPlayer();
void initializeGrid();
char getCurrPlayer();
bool isNoWinner();
bool endGameTest();
void testReset();
char getGameGridElem();
char getCurrPlayer();
bool horizontalWinTest();
void horizontalTestData();
void verticalTestData();
void diagonalForwardTestData();
void diagonalBackwardTestData();
bool verticalWinTest();
bool diagonalWinTest();
bool backwardDiagonalWin();
bool forwardDiagonalWin();
void testDataHorizontal();
void testDataVertical();
void testDataDiagonalForward();
void testDataDiagonalBackward();

using namespace std;

int main()
{ 
  gameLogic();
  gameLoop();
  cin.get();
  cin.get();
  return 0;
}

void printGrid()
{
  cout << "----------------" << endl << endl;
  for(int i = 0; i < GRIDSIZE; i++)
  {
    cout << " ";
    for(int j = 0; j < GRIDSIZE; j++)
    {
      cout << "| " << gameGrid[i][j];
    }
    cout << "| ";
    cout << endl;
    cout << "         " << endl;
  }
  cout << "----------------" << endl;
}


void playAgain()
{
  char answer = 'N';
  if(isNoWinner() == true)
  {
    cout << "Draw: No Winner " << endl;
  }
  else 
  {
    cout << "Congratulations You Win Player" << "   " <<  getCurrPlayer()  <<  endl << endl;
  }
  cout << "Would you like to play again?  'Y' or 'N':   " << endl;
  cin >> answer;
  if(answer == 'Y' || answer == 'y')
  {
    cout << endl << endl << endl << endl << endl
         << "_____________________________________" 
         << endl << "ROUND TWO " << endl << endl << endl;
    resetGame();
  }
  else exit(EXIT_SUCCESS);
}

//validate Col data
void validateColData() 
{   
  //handle out of bounds
  bool valid = true;
  int col = getCol();

  if(col < 0 || col > 2)
  {
    valid = false;
  }
    
  while(valid == false)
  {
    //handle out of bounds
    cout << "Invalid Input:   Enter a COL" << endl;
    col = getColInput();

    if(col > -1 && col < 3)
    {
      valid = true;
    }
  }
}

////validateData
 void validateRowData()
{
  bool valid = true;
  int row = getRow();

  if(row < 0 || row > 2)
  {
    valid = false;
  }
    
  while(valid == false)
  {
    //handle out of bounds
    cout << "Invalid Input:   Enter a ROW" << endl;
    row = getRowInput();

    if(row > -1  && row < 3)
    {
      valid = true;
    }
  }
}

 int getColInput()
{
  int col;
  cout << "Enter a COl (between 0 and 2)" << endl;
  cin >> col;
  setCol(col);
  cout << endl;
  return col;
}

//
 int getRowInput()
{
  int row;
  cout << " Enter a ROW (between 0 and 2)" << endl;
  cin >>  row;
  setRow(row);
  cout << endl;
  return row;
}

 //input playerData
void playerInputLoop()
{
  bool player = getPlayer();
 // cout << "Player Input Loop:  " << endl;
 //player true = player one
  if(player)
  {
    cout << "PLAYER 1:  " << endl << endl;
  }
  else
  {
    cout <<  "PLAYER 2:  " << endl << endl;
  }
  int row = getRowInput();
  validateRowData();
  int col = getColInput();
  validateColData();
  turnCounter++;
cerr << "turn" << turnCounter << endl;
}

void gameLogic()
{
//cerr << "gameLogic" << endl;
  gameGrid = new char*[GRIDSIZE];
  for( int i=0; i<GRIDSIZE; ++i)
  {
    gameGrid[i] = new char[GRIDSIZE];
  } 
 
  row = 0;
  col = 0;
  player = true; //P1 == true == X
  //turnCounter = 1; //FOR P2 ONLY for tiebreaker
}

//getTurnCounter
int getTurnCounter()
{
  return turnCounter;
}

//takes 1 if increment or 0 for decrement
void setTurnCounter(int counter)
{
  turnCounter++;
}

   //set player move in gameGrid
bool setPlayerMove()
{
  //cout << "setPlayerMove-Player:  " << player << endl;
  //if non empty space then reprompt user
  if(gameGrid[row][col] != ' ' )
     return false;
  if(player)
  {
    gameGrid[row][col] = 'X';
  }
  else
  {
    gameGrid[row][col] = '0';
  }
  return true;
}

void setCol(int c)
{
  col = c;
}

void setRow(int r)
{
  row = r;
}

int getCol()
{
  return col;
}

int getRow()
{
  return row;
}

//main Game Loop
void gameLoop()
{
  cout << endl << "TicTacToe" << endl;
  initializeGrid();
  printGrid();

  if(endGameTest())
  {
    playAgain();
  }

  while(true)
  {
    playerInputLoop();

    while(!setPlayerMove())
    {
      playerInputLoop();
    }
    printGrid();

    if(endGameTest())
    {
      playAgain();
    }
    switchPlayer();
  }
}

//reset game grid to default value "  " for replay
void resetGrid()
{
  for(int i = 0; i <= 2; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      gameGrid[i][j] = ' ';
    }
  }
}

//handle endgame
void resetGame()
{
  resetGrid();
  gameLoop();
}

bool getPlayer()
{
  return player;
}

void switchPlayer()
{
 //cout << "switch player called" << endl;

  //if player1(true) then switch to P2
  if(player)
  {
    player = false;
  }
  else
  {
    player = true;
  }
}

//initialize game grid to " "
void initializeGrid()
{
//cerr << "initializeGrid" << endl;
  for(int i = 0; i <= 2; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      gameGrid[i][j] = ' ';
    }
  }
}

//handles tie win conditon
bool isNoWinner()
{
  int drawCount = 9;
  //if player 2 makes last move and no win
  if(getTurnCounter() == drawCount )
  {
    return true;
  }
  return false;
}

//handle endGame
bool endGameTest()
{
  if(diagonalWinTest()  == true || verticalWinTest() == true || horizontalWinTest() == true || isNoWinner() == true)
  {  
    return true;
  }
  return false;
}

void testReset()
{
  initializeGrid();
}

char getGameGridElem(int row  ,int col)
{
  return gameGrid[row][col];
}

char getCurrPlayer()
{
  return currPlayer;
}

bool horizontalWinTest()
{
  int count = 0;
  currPlayer = 'X'; //player 1
   //if player2 switch char value
  if(!player)
  {
    currPlayer = 'O';
  }
   //
  for(int i = 0; i <= 2; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      if(gameGrid[i][j] == currPlayer)
      {
        count++;
      }
      else
      {
        count = 0;
      }
      if(count == 3)
      {
        return true;
      }
    }
  }
  return false;
}

void horizontalTestData()
{
  gameGrid[0][0] = 'X';
  gameGrid[0][1] = 'X';
  gameGrid[0][2] = 'X';
}

void verticalTestData()
{
  gameGrid[0][1] = 'X';
  gameGrid[1][1] = 'X';
  gameGrid[2][1] = 'X';
}
void diagonalForwardTestData()
{
  gameGrid[0][2] = 'X';
  gameGrid[1][1] = 'X';
  gameGrid[2][0] = 'X';
}

void diagonalBackwardTestData()
{
  gameGrid[0][0] = 'X';
  gameGrid[1][1] = 'X';
  gameGrid[2][2] = 'X';
}

bool verticalWinTest()
{
  int count = 0;
  currPlayer = 'X';

  if(!player)
  {
    currPlayer = 'O';
  }

  for(int i = 0; i <= 2; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      if(gameGrid[j][i] == currPlayer)
      {
        count++;
      }
      else
      {
        count = 0;
      }
      if(count == 3)
      {
        return true;
      }
    }
  }
   return false;
}

bool backwardDiagonalWin()
{
  //backward slash diag win
  if(!player)
  {
    if(gameGrid[0][0] == 'O' && gameGrid[1][1] == 'O' && gameGrid[2][2] == 'O' )
    {
      return true;
    }
  }
  else
  {
    if(gameGrid[0][0] == 'X' && gameGrid[1][1] == 'X' && gameGrid[2][2] == 'X' )
    {
      return true;
    }
  }
  return false;
}

bool forwardDiagonalWin()
{
  if(!player)
  {
    if(gameGrid[0][2] == 'O' && gameGrid[1][1] == 'O' && gameGrid[2][0] == 'O' )
    {
      return true;
    }
  }
  else //if player 1
  {
    if(gameGrid[0][2] == 'X' && gameGrid[1][1] == 'X' && gameGrid[2][0] == 'X' )
    {
      return true;
    }
  }
  return false;
}

bool diagonalWinTest()
{
  if(backwardDiagonalWin() == true || forwardDiagonalWin() == true)
  {
    return true;
  }
  return false;
}

//____________________
//Testing functions
void testDataVertical()
{
  for(int i = 0; i < 1; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      if(!player)
      {
        gameGrid[j][i] = 'O';
      }
      else
      {
        gameGrid[j][i] = 'X';
      }
    }
  }
}

void testDataHorizontal()
{
  for(int i = 0; i < 1; i++)
  {
    for(int j = 0; j <= 2; j++)
    {
      if(!player)
      {
        gameGrid[j][i] = 'O';
      }
      else
      {
        gameGrid[j][i] = 'X';
      }
    }
  }
}

// '/'forward slash horiz test
void testDataDiagonalForward()
{
  gameGrid[0][2] = 'O';
  gameGrid[1][1] = 'O';
  gameGrid[2][0] = 'O';
}


// '\'backward slash horiz test
void testDataDiagonalBackward()
{
  gameGrid[0][0] = 'O';
  gameGrid[1][1] = 'O';
  gameGrid[2][2] = 'O';
}

//END TESTING
//____________________

